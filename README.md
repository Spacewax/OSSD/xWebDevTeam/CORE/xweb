# xWeb

The xWeb Project is starting as a fork from XOOPS 2.0.18.2 - from  [SourceForge.net](https://sourceforge.net/projects/xoops/files/XOOPS%20Core%20%28stable%20releases%29/XOOPS%202.0.18.2/). Because of this, many files in the code base comes from The XOOPS Project and were originally copyrighted as such.

Every file that was originally created by The XOOPS Project but then edited for *xWeb* will see its header comment block edited, to replace the original XOOPS comment block by a new smaller one. Every single file created by XOOPS but edited by xWeb is and will always display in its comment block, a @copyright and @author tags to mention the file originally came from XOOPS.

An example of such file would be, *htdocs/index.php*. This is what the new comment block on header may look like.

```php
/**
*
* @copyright	http://www.xoops.org/ The XOOPS Project
* @copyright	XOOPS_copyrights.txt
* @copyright	http://xweb.spacewax.net/ The xWeb Project
* @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License (GPL)
* @package		core
* @since		XOOPS
* @author		http://www.xoops.org The XOOPS Project
* @author		modified by hyperclock <hyperclock@xweb.spacewax.net>
* @version		1.0.x-dev
*
*/
```
